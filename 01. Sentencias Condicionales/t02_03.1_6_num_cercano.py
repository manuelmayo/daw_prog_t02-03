#t02_03.1_6_num_cercano

numeros = input("Introduzca 5 números. Separados por comas: ")

try:
	lnumeros = numeros.split(",")
	if len(lnumeros) == 5:
		lnumeros = [float(x) for x in lnumeros]
		pnum = lnumeros[0]
		cola = lnumeros[1:5]
		listdiff = [0,0,0,0]
		for i in range(4):
			listdiff[i] = abs(pnum - cola[i])
		#A diferencia máis pequena
		mindiff = min(listdiff)
		#Para saber cantos números hai coa diferencia máis baixa
		listindex = []
		for n in range(4):
			if listdiff[n] == mindiff:
				listindex.append(n)
		#Sacamos los números
		listminimos = [cola[x] for x in listindex]
		listminimos = list(set(listminimos))
		if len(listminimos) == 1:
			cercano = listminimos[0]
			print("El número más cercano a {0} es {1}".format(pnum,cercano))
		else:
			cercano1 = listminimos[0]
			cercano2 = listminimos[1]
			print("Los números más cercanos a {0} son: {1} y  {2}".format(pnum,cercano1,cercano2))
	else:
		print(">> No hai 5 números")
except ValueError:
	print(">> Error con los datos introducidos")