#t02_03.1_3_mayor_de_3

n1 = input("Introduzca el primer número: ")
n2 = input("Introduzca el segundo número: ")
n3 = input("Introduzca el tercer número: ")

try:
	fn1, fn2, fn3 = float(n1), float(n2), float(n3)
	#nmaior = max([fn1,fn2,fn3])
	nmaior = fn1
	if nmaior < fn2:
		nmaior = fn2
	if nmaior < fn3:
		nmaior = fn3
	print(">> El número mayor es el {0}".format(nmaior))
except ValueError:
	print(">> Error con los datos introducidos")