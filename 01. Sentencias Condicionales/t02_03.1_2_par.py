#t02_03.1_2_par

num = input("Introduzca un número entero: ")

if num.isdecimal():
	inum = int(num)
	if inum == 0:
		print("0 es un número impar")
	elif inum % 2 == 0:
		print("{0} es un número par".format(num))
	else:
		print("{0} es un número impar".format(num))
else:
	print("El valor '{0}' no es un número entero".format(num))