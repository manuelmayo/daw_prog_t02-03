#t02_03.1_7_dni_letra

dni = input("Introduzca los números del DNI: ")

try:
	if len(dni) == 8 and dni.isdecimal():
		tabla = "TRWAGMYFPDXBNJZSQVHLCKE"
		intdni = int(dni)
		resto = intdni%23
		letra = tabla[resto]
		print(">> La letra del DNI es: ",letra)
		print(">> {0}-{1}".format(dni,letra))
	else:
		print(">> Error. Debe tener 8 dígitos")
except ValueError:
	print(">> Error con los datos introducidos")