#t02_03.1_5_orden_palabra

palabras = input("Introduzca 5 palabras. Separadas por espacios: ")

try:
	lpalabras = palabras.split()
	if len(lpalabras) == 5:
		lpalabras.sort()
		primera = lpalabras[0]
		print(">> La primera palabra por orden alfabético es '{0}'".format(primera))
	else:
		print(">> No hai 5 palabras")
except ValueError:
	print(">> Error con los datos introducidos")