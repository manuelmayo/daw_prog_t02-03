#t02_03.1_1_multiplos

n1 = input("Introduzca el primer número: ")
n2 = input("Introduzca el segundo número: ")

try:
	fn1 = float(n1)
	fn2 = float(n2)
	if fn1 % fn2 == 0:
		print(n1+" es múltiplo de "+n2)
		#print("{0} es múltiplo de {1}".format(n1,n2))
	else:
		print(n1+" no es múltiplo de "+n2)
		#print("{0} no es múltiplo de {1}".format(n1,n2))
except:
	print("Error cos datos introducidos")