#t02_03.3_4_texto_fin2

end = False

print("Escribe lo que quieras (END para finalizar):")

while not end:
	try:
		entrada = input()
		if "FIN" in entrada:
			end = True
			print("Eeehh!!! Escribiches 'FIN'... Ciao!")
	except KeyboardInterrupt:
		print("Oooooops!! FIN para finalizar")
		#exit()
	except EOFError:
		end = True
		print("Eeehh!!! End-Of-File... Ciao!")
		#exit()