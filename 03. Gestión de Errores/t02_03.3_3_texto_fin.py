#t02_03.3_3_texto_fin

end = False

print("Escribe lo que quieras (END para finalizar):")

while not end:
	try:
		entrada = input()
		if "FIN" in entrada:
			end = True
			print("Eeehh!!! Escribiches 'FIN'... Ciao!")
	except KeyboardInterrupt:
		print("Oooooops!! FIN para finalizar")