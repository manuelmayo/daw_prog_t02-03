#t02_03.3_1_sumador_fich

file_name = "t02_03.3_1_sumador_fich_file.txt"

file = open(file_name)

texto = file.read()
lineas = texto.split("\n")

sumando = 0
nlinea = 0

for l in lineas:
	nlinea += 1
	for n in l.split():
		try:
			sumando += float(n)
		except ValueError:
			print(">> Linea {0}. Error. '{1}' no es un número válido".format(nlinea,n))

print("Archivo: ", file_name)
print("El resultado de la suma de todos los números del archivo es: ",sumando)