#t02_03.3_2_ecu_2nd_grd_error

import math

print("Ecuación: a·x²+b·x+c=0")

ca = input("Introduzca el valor del coeficiente 'a': ")
cb = input("Introduzca el valor del coeficiente 'b': ")
cc = input("Introduzca el valor del coeficiente 'c': ")

try:
	ca, cb, cc = float(ca), float(cb), float(cc)
	discr = cb**2-4*ca*cc
	if discr > 0:
		try:
			x1 = (-cb + math.sqrt(discr))/(2*ca)
			x2 = (-cb - math.sqrt(discr))/(2*ca)
			print(">> Las soluciones son:")
			print("\t x1 = {0:.2f}".format(x1))
			print("\t x2 = {0:.2f}".format(x2))
		except ZeroDivisionError:
			print("ZeroDivisionError: El coeficiente 'a' debe ser distinto de 0")
	elif discr == 0:
		try:
			x1 = -cb/(2*ca)
			print(">> La solución es:\n\t x = {0:.2f}".format(x1))
		except ZeroDivisionError:
			print("ZeroDivisionError: El coeficiente 'a' debe ser distinto de 0")
	elif discr < 0:
		print("El discriminante '√b²−4·a·c' es negativo. No tiene soluciones reales.")
except ValueError:
	print(">> Error con los datos introducidos")