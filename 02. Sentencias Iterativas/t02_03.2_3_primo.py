#t02_03.2_3_primo

num = input("Introduce un número entero positivo: ")

try:
	if num.isdecimal():
		num = int(num)
		if num > 0:
			primo = True
			for n in range(2,int(num/2)):
				if num%n==0:
					primo = False
					break
			if primo:
				print(">> El número {} es primo".format(num))
			else:
				print(">> El número {} no es primo".format(num))
		else:
			print("Error. El número debe ser positivo")
	else:
		print("Error. No has introducido un número entero")
		
except ValueError:
	print(">> Error con los datos introducidos")