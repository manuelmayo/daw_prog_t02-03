#t02_03.2_9_palindromo

import re

texto = input("Escribe un texto: ")
inverso = texto[::-1]

tildes = "áéíóú"
vocales = "aeiou"

trans = str.maketrans(tildes,vocales)

#Convetimos a minúsculas y cambiamos las vocales con tildes por vocales sin tildes
texto = texto.lower().translate(trans).replace(" ","")
inverso = inverso.lower().translate(trans).replace(" ","")

#Eliminamos carácteres no alfanuméricos
texto = re.sub("[^0-9a-zA-Z]+","",texto)
inverso = re.sub("[^0-9a-zA-Z]+","",inverso)

print(texto)
print(inverso)

if texto == inverso:
	print("Es un palíndromo! :D")
else:
	print("No es un palíndromo :(")