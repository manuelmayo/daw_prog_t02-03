#t02_03.2_2_tabla_completa

print("{0}\n* TABLA DE MULTIPLICAR *\n{0}".format("*"*24))

for x in range(11):
	print("{1}\n{0:<6}TABLA DEL {2}\n{1}".format("","-"*22,x))
	for n in range(11):
		print("{0:<6}{1} x {2} = {3}".format("",x,n,x*n))
print("-"*22)