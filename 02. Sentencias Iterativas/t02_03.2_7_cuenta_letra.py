#t02_03.2_7_cuenta_letra

caracter = input("Escribe un carácter: ")

if len(caracter) == 1:
	print("Escribe lo que quieras (<> para finalizar)")
	texto = ""
	end = False
	while not end:
		entrada = input()
		texto += entrada
		if "<>" in entrada:
			end = True
	print("Número de apariciones del cáracter '{0}': {1}".format(caracter,texto.count(caracter)))
else:
	print("Error. Debes escribir un carácter")