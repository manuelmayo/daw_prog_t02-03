#t02_03.2_10_cesar

print("Introduce el mensaje (#FIN# para terminar):")

texto = ""
end = False
while not end:
	entrada = input()
	if entrada == "#FIN#":
		end = True
	else:
		texto += entrada+"\n"
		
desp = int(input("Introduce el desplazamiento: "))

tcifrado = ""
texto = texto.upper()
for c in texto:
	if c.isalpha():
		unicode = ord(c)
		newunicode = unicode + desp
		if newunicode > 90:
			newunicode -= 25
		elif newunicode < 65:
			newunicode += 25
		caracter = chr(newunicode)
	else:
		caracter = c
	tcifrado += caracter
	
print("\nCriptograma -->\n")
print(tcifrado)