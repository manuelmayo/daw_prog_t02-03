#t02_03.2_4_mas_primos

num = input("Números primos entre 1 y el número introducido: ")

def esprimo(n):
	primo = True
	for x in range(2,int(n/2)):
		if n%x==0:
			primo = False
			break
	return primo

try:
	if num.isdecimal():
		num = int(num)
		if num > 1:
			print(">> Números primos entre 1 y {}:".format(num))
			for x in range(2,num):
				if esprimo(x):
					print(x)
		else:
			print("Error. El número debe ser positivo y mayor que 1")
	else:
		print("Error. No has introducido un número entero")
		
except ValueError:
	print(">> Error con los datos introducidos")