#t02_03.2_5_reves

frase = input("Escribe una frase: ")

try:
	fraseinver = frase[::-1]
	numpalabras = len(frase.split())
	print("Frase del revés: ",fraseinver)
	print("Número de palabras: ",numpalabras)
except ValueError:
	print(">> Error con los datos introducidos")