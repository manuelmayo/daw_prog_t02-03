#t02_03.2_1_tabla_num

num = input("Introduce un número entero entre 0 y 9: ")

try:
	num = int(num)
	if 0 <= num <= 9:
		print("TABLA DE MULTIPLICAR DEL ",num)
		for n in range(10):
			print("{0} x {1} = {2}".format(num,n,num*n))
	else:
		print("Error. No has introducido un número entero entre 0 y 9")
except ValueError:
	print(">> Error con los datos introducidos")